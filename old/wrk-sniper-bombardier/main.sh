#!/bin/bash

IP=$1

python3 test_file_generate.py -mt $2 -sc $3 -sr $4

COUNT=1
while IFS= read -r CONN REQUEST; do
    echo "Test $COUNT/$2"
    ./script-trabalho01-trps.sh $IP $CONN $REQUEST
    let "COUNT++"
done <"test_file.txt"
echo "Done"

# Extract the average time for all files in the output directory
for output_file in $( ls output); do
	cat output/$output_file | grep Latency | awk '{print $2}' | awk -F ms '{print $1}' >> average/$output_file
done

# Generate plot images
for output_file in $( ls average); do
    gnuplot -e "set terminal png; set output 'images/$output_file.png'; plot 'average/$output_file' with lines;"
done

rm test_file.txt