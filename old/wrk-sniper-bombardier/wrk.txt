Running 10s test @ https://google.com
  1 threads and 1 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   604.89ms   87.86ms 703.51ms   62.50%
    Req/Sec     1.19      0.40     2.00     81.25%
  Latency Distribution
     50%  611.39ms
     75%  694.90ms
     90%  702.87ms
     99%  703.51ms
  16 requests in 10.01s, 12.34KB read
Requests/sec:      1.60
Transfer/sec:      1.23KB
