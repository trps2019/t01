import argparse
import os
# conexoes e requisições

ap = argparse.ArgumentParser()
ap.add_argument("-mt", "--max_test", type=int, required=True,
                help="Number max of tests")
ap.add_argument("-sc", "--start_connection", type=int, required=True,
                help="Start numbers of connections to test")
ap.add_argument("-sr", "--start_request", type=int, required=True,
                help="Start numbers of requests to test")

args = vars(ap.parse_args())

def generate_file():
    star_conn = args["start_connection"]
    start_request = args["start_request"]
    with open("test_file.txt", "w") as f:
        for i in range(args["max_test"]):
            f.write("{} {}\n".format(star_conn, start_request))
            star_conn += 10
            start_request += 10
            
generate_file()
