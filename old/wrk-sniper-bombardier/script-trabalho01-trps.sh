#!/bin/bash

latency_test()
{
    IP=$1
    echo "Time for connection on $IP..."
    wrk -d 10s -t 1 -c 1 --latency $IP  > wrk.txt
    echo -e "\t\tAvg\tStdev\tMax\t+/- Stdev"
    cat wrk.txt | grep Latency | grep %
    echo -e "\n"
}

for CMD in xterm wrk cat
do
    if [ ! `which $CMD` ]
    then
        echo "[ERROR] Missing command/app \"$CMD\". Install it first."
        exit
    fi
done

[ $1 ] || { echo "Usage: $0 <ip> <connections_number> <request_number>";exit; }

IP=$1
CONN_NUMBER=$2
REQUEST_NUMBER=$3

echo "Generate network latency test before attacks..."
latency_test $IP "average:" >> "output/latency_before_attacks.txt"

echo "Generate Bombardier attack..."
xterm -e "timeout 13s bombardier -c $CONN_NUMBER -n $REQUEST_NUMBER $IP" & 
latency_test $IP "average time for bombardier attack:" >> "output/bombardier_attack.txt"

echo "Generate Sniper attack..."
xterm -e "timeout 13s sniper -c $CONN_NUMBER -n $REQUEST_NUMBER $IP" & 
latency_test $IP "average time for tcp syn flood:" >> "output/sniper_attack.txt"

echo "Generate network latency test after attacks..."
latency_test $IP "average:" >> "output/latency_after_attacks.txt"
