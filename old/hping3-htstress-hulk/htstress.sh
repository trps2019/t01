#!/bin/bash
[ $1 ] && [ $2 ] || { echo "Uso: $0 <ip> <repeticoes>";exit;}

IP=$1
REP=$2

hping3 $IP -c $REP | grep 'round-trip min/avg/max = ' | awk '{print $2 $3 $4 $5}'

cd ~/htstress/

./htstress -c $REP -t 2 $IP >> /dev/null &


hping3 $IP -c $REP | grep 'round-trip min/avg/max = ' | awk '{print $2 $3 $4 $5}'
