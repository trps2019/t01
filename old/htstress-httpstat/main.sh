#!/bin/bash


python test_file_generate.py -t $1 

COUNT=1
while IFS= read -r NUM; do
    echo "Test $COUNT/$1"
    ./run.sh $NUM $2 $3
    let "COUNT++"
done <"test_file.txt"
echo "Done"



# Generate plot images
for output_file in $( ls average); do
    gnuplot -e "set terminal png; set output 'images/$output_file.png'; plot 'average/$output_file' with lines;"
done