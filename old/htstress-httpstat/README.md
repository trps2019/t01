# Ferramentas
## Geração de carga
htstress - Ferramenta de stress de servidores HTTP
Disponivel em: https://github.com/arut/htstress

## Medição de tempo
httpstat - Ferramenta em python para medição tempo de resposta de varias informações do http
(dentre elas o tempo de resolver o DNS, SSL Handshake e etc), foi considerado o tempo total para os teste
Disponivel em: https://github.com/reorx/httpstat ou via pypi usando pip install httpstat

## Medição da latencia da rede:
ping - ferramenta nativa de sistemas operacionais linux.
Disponivel em qualquer distribuição linux e em outros lugares

#Metogologia

## Medição de latencia da rede
usa a ferramenta ping com o parametro -c que representa a quantidade de vezes que o teste sera feito e se captura o valor medio que a propria ferramenta calcula

##Medição de tempo de resposta sem carga:
chama a ferramenta httpstat o numero de vezes recebido por parametro e a cada execução, captura-se o tempo total e se concatena em um arquivo
depois que todas as execuções são feitas é realizado a soma dos valores e dividido pelo numero de vezes para calcular a media

##Medição de tempo de resposta com carga:
Assim como o metodo a cima usa o httpstat realiza as somas e a divizão porem antes de tudo isso dispara um gnome-terminal paralelo que fica fazendo requisições ao servidor HTTP

serão feitas 10000 vezes o numero de testes em requisiçoes

#Uso da ferramenta:

##Instalar:
rodar install.sh ira clonar o repositorio do htstress e baixar via pip o httpstat

##Rodar:
roda o run.sh com os parametros: <numero_de_testes> <ip_servidor> <porta_servidor>

numero de tempos de resposta a serem capturados = <numero_de_testes>
numero maximo de requisições que serão feitas ao servidor = 10000 * <numero_de_testes>
após os testes de tempo de resposta o proprio programa encerra as requisições ao servidor (ainda que nao tenham sido todas as requisiçoes)


#Nossos Resultados:
rodando em servidor em lan

maquina atacante:
Phenon II X2 3.2GHz 8GB RAM linux mint 19

maquina servidor:
Intel Core i5 5200u 8GB RAM Debian Gnome 10

latencia media da rede 1.594
tempo de resposta medio sem carga: 5ms
tempo de resposta medio com carga: 57



