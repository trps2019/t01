#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Usage $0 <numero_de_testes> <ip_servidor> <porta_servidor>"
    exit
fi

echo "The server requests will be made until the max number of $((10000 * $1)) or until the end of the tests"

#calcula a latencia da rede com ping
echo "Network latency test..."
mediatempo=$(ping $2 -c $1 | grep avg | awk -F "=" '{print $2;}' | awk -F "/" '{print $2;}')
echo "$mediatempo" >> average/average_latency.txt

#calcula o tempo de resposta do servidor sem nenhuma carga
echo "Loadless network test..."
for i in $(seq 1 $1)
do
	httpstat $2:$3 | grep total | awk -F ":" '{print $2;}' | awk -F "ms" '{print $1;}' >> output/test_loadless.txt
done 

somatempos=$(cat output/test_loadless.txt | paste -sd + -)
mediatempo=$(echo "$(($somatempos)) / $1" | bc)
echo "$mediatempo" >> average/average_loadless.txt

#dispara o gerador de carga em paralelo
gnome-terminal --window -- ./htstress/htstress  -n $((10000 * $1)) -c 100 -t 2 -d $2

#enquanto o gerador roda calcula o tempo de resposta
echo "With load network test..."
for i in $(seq 1 $1)
do
	httpstat $2:$3 | grep total | awk -F ":" '{print $2;}' | awk -F "ms" '{print $1;}' >> output/test_with_load.txt
done 

somatempos=$(cat output/test_with_load.txt | paste -sd + -)
mediatempo=$(echo "$(($somatempos)) / $1" | bc)
echo "$mediatempo" >> average/average_with_load.txt

#mata o gerador
killall htstress

