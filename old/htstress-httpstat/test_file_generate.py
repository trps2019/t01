import argparse
import os
# conexoes e requisições

ap = argparse.ArgumentParser()
ap.add_argument("-t", "--tests_number", type=int, required=True,
                help="Number of the tests do generate")

args = vars(ap.parse_args())

def generate_file():
    test = args["tests_number"]
    with open("test_file.txt", "w") as f:
        for i in range(args["tests_number"]):
            f.write("{}\n".format(test))
            test *= 10
            
generate_file()
