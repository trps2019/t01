#!/bin/bash

ping()
{
	IP=$1
	PRINT=$2
	echo "Calculando tempo de conexao para $IP..."
	httpstat $IP $REP > httpstat.txt
	echo "Tempo $PRINT" `cat httpstat.txt | grep total | awk -F ':' '{print $2}'`
}


[ $1 ] || { echo "Uso: $0 <ip>";exit; }

if [ ! -d 'htstress' ]
then
	sudo bash install.sh
fi

IP=$1

echo "============================="
ping $IP $REP "medio:"
echo "============================="

echo ""
echo "Proximo teste: htstress"
echo ""

echo "============================="
echo "Ataque htstress iniciado"
xterm -e "./htstress/htstress -n 100000000000000000 -c 125 $IP" &
sleep 3
ping $IP $REP "medio com ataque htstress:"
echo "============================="
