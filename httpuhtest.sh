#!/bin/bash

req=100
server="localhost"
car_server=""
lat_server=""

usage(){
    echo "How to use:"
    echo "$0 <options>:"
    echo ""
    echo "Avaliable Options:"
    echo "-s <servername> (REQUIRED)"
    echo "-l <latency_method>"
    echo "-c <charge_method>"
    echo "-r <number_of_requisitions>"
    echo ""
    echo "Accepted Latency Methods: hping3, httping, wrk, httpstat"
    echo "Accepted Charge Methods: hping3, hulk, htstress, sniper, slowloris, bombardier"
    exit
}

charge(){
    #recebe uma string ferramenta em $1 executa o script correto de gerador de carga
    #recebe uma string do servidor a ser atacado em $2
    #recebe um inteiro, numero de requisições no $3, optional
    #TODO
    return
}

latency(){
    #recebe uma string ferramenta em $1 e executa o script correto de teste de latencia
    #recebe uma string do servidor a ser testado em $2
    #recebe um inteiro, numero de requisições no $3
    #TODO
    return
}



while getopts ":s:r:c:l:" opt; do
  case $opt in
    s)
        echo "Server: $OPTARG" >&2
        server="$OPTARG"
        ;;
    c)
        echo "Charge Method: $OPTARG" >&2
        car_server="$OPTARG"
        ;;
    l)
        echo "Latency Method: $OPTARG" >&2
        lat_server="$OPTARG"
        ;;
    r)
        echo "Number of Requisitions: $OPTARG" >&2
        req="$OPTARG"
        ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        usage
        exit 1
        ;;
  esac
done

if [ car_server ] 
then
    charge $car_server $server $req
fi

if [ lat_server ]
then
    latency $lat_server $server $req
fi

exit 0;